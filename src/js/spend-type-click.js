$(".spend-type-item").on("click", function() {
    currentType = this.classList[1];
    type        = "spending";
    if ($(".sidebar").css("display") == "none") {
        $(".sidebar-btn-up").click();
        $(".go-to-input")[0].click();
    } else {
        if ($(".container-for-show").css("display") == "block") {
            $(".info-for-type-title").text("Расходы за месяц");
            $(".container-for-show").css("background", "rgb(247,172,180, .1)");
            $(".main-input-img").attr("src", "src/img/" + currentType + ".png");
        } else {
            $(".sidebar-list-info").hide();
            $(".container-for-show").slideToggle(500);
            $(".go-to-input").hide();
            $(".back-to-list-info").removeClass("d-none");
            $(".info-for-type-title").text("Расходы за месяц");
            $(".container-for-show").css("background", "rgb(247,172,180, .1)");
            $(".main-input-img").attr("src", "src/img/" + currentType + ".png");
        }
    }
});
$(".container .spend-type-item").hover(
    function() {
        $($(this).children()[1]).show(100);
        $($(this).children()[0]).animate({"width": "60px"}, 100);
    },
    function() {
        $($(this).children()[1]).hide();
        $($(this).children()[0]).animate({"width": "96px"}, 100);
    }
);

$(".sidebar .spend-type-item").hover(
    function() {
        $($(this).children()[1]).show(100);
    },
    function() {
        $($(this).children()[1]).hide();
    }
);
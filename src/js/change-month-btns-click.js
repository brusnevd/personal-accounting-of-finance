$(".btn-month-back").on("click", function() {
    nextMonth = currentMonthRus;
    currentMonthRus = previousMonth;
    currentMonth = monthes[monthesRus.indexOf(currentMonthRus)];
    if (monthesRus.indexOf(previousMonth) - 1 == -1) {
        previousMonth = "ноябрь";
    } else {
        previousMonth = monthesRus[monthesRus.indexOf(previousMonth) - 1];
    }
    $(".back-month-name").text(previousMonth.toUpperCase());
    $(".current-month").text(currentMonthRus.toUpperCase());
    $(".forward-month-name").text(nextMonth.toUpperCase());

    $(".sidebar-list-info").children().remove();

    eachMonthInfo[currentMonth].list.forEach(element => {
        let item = createItem(element);
        $(".sidebar-list-info").append(item);        
    });

    $(".info-for-type-container").children().remove();

    eachMonthInfo[currentMonth].list.forEach(element => {
        if (element.type == type) {
            let item = createItem(element);
            item.querySelector("#btnDelete" + element.id).remove();
            $(".info-for-type-container").append(item);
        }        
    });
    
    setCirclesValues();
});

$(".btn-month-forward").on("click", function() {
    previousMonth = currentMonthRus;
    currentMonthRus = nextMonth;
    currentMonth = monthes[monthesRus.indexOf(currentMonthRus)];
    if (monthesRus.indexOf(nextMonth) + 1 == 14) {
        nextMonth = "февраль";
    } else {
        nextMonth = monthesRus[monthesRus.indexOf(nextMonth) + 1];
    }
    $(".back-month-name").text(previousMonth.toUpperCase());
    $(".current-month").text(currentMonthRus.toUpperCase());
    $(".forward-month-name").text(nextMonth.toUpperCase());

    $(".sidebar-list-info").children().remove();

    eachMonthInfo[currentMonth].list.forEach(element => {
        let item = createItem(element);
        $(".sidebar-list-info").append(item);        
    });

    $(".info-for-type-container").children().remove();

    eachMonthInfo[currentMonth].list.forEach(element => {
        if (element.type == type) {
            let item = createItem(element);
            item.querySelector("#btnDelete" + element.id).remove();
            $(".info-for-type-container").append(item);
        }        
    });

    setCirclesValues();
});
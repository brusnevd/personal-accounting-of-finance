var symbolsAfterPoint = 0, flag = 0;
$(".symbol").on("click", function() {
    let inputLength         = $(".main-input").val().length,
        symbol              = $(this).text();

    if (inputLength <= 10 && symbol != "←" && symbol != "." && !$(".main-input").val().includes(".")) {
        if (inputLength == 0 && symbol == "0") {
            $(".main-input").val(
                $(".main-input").val().slice(0, inputLength - 2) + symbol + ".0 $"
            );
            symbolsAfterPoint = 0;
            flag = 1;
        }
        if ($(".main-input").val() == "0 $") {
            $(".main-input").val("0 $");
        } else {
            $(".main-input").val(
                $(".main-input").val().slice(0, inputLength - 2) + symbol + " $"
            );
        }
    } else if (inputLength <= 10 && symbol != "←" && symbol != "." && $(".main-input").val().includes(".")) {
        if (symbolsAfterPoint == 1) {
            $(".main-input").val(
                $(".main-input").val().slice(0, inputLength - 3) + symbol + " $"
            );
            symbolsAfterPoint = 2;
        }

        if (symbolsAfterPoint == 0) {
            $(".main-input").val(
                $(".main-input").val().slice(0, inputLength - 4) + symbol + "0 $"
            );
            flag = 1;
            symbolsAfterPoint = 1;
        }
    }

    if (symbol == "←" && inputLength > 2) {
        if (symbolsAfterPoint == 1) {
            symbolsAfterPoint = 0;
            console.log('sss');
            $(".main-input").val(
                $(".main-input").val().slice(0, inputLength - 4) + "00 $"
            );

        } else if (symbolsAfterPoint == 2) {
            symbolsAfterPoint = 1;
            $(".main-input").val(
                $(".main-input").val().slice(0, inputLength - 3) + "0 $"
            );
        } else if (flag == 1) {
            $(".main-input").val(
                $(".main-input").val().slice(0, inputLength - 5) + " $"
            );
            flag = 0;
        } else {
            $(".main-input").val(
                $(".main-input").val().slice(0, inputLength - 3) + " $"
            );
        }
    }

    if (symbol == "←" && inputLength == 3) {
        $(".main-input").val("");
    }

    if (symbol == ".") {
        if (!$(".main-input").val().includes(".") && inputLength > 0) {
            $(".main-input").val(
                $(".main-input").val().slice(0, inputLength - 2) + symbol + "00 $"
            );
            flag = 1;
        }
    }
})
var eachMonthInfo = {
    january: {
        list: [
            // {
            //     id: 1,
            //     type: "spending",
            //     typeRus: "связь",
            //     typeImg: "phone",
            //     money: "500",
            // }
        ],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
    february: {
        list: [],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
    march: {
        list: [],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
    april: {
        list: [],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
    may: {
        list: [],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
    june: {
        list: [],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
    july: {
        list: [],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
    august: {
        list: [],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
    september: {
        list: [],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
    october: {
        list: [],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
    november: {
        list: [],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
    december: {
        list: [],
        currentBalance: 0,
        incomeBalance: 0,
        spendsBalance: 0,
    },
}

var translator = {
    phone: "связь",
    bus: "транспорт",
    sedan: "автомобиль",
    company: "проживание",
    cutlery: "кафе",
    towel: "гигиена",
    ["grocery-bag"]: "продукты",
    exercise: "спорт",
    sweater: "одежда",
    taxi: "такси",
    clinic: "лекарства",
    confetti: "развлечения",
    spending: "личные расходы",
    income: "Доход",
}

var monthes = ["december", "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december", "january"];
var monthesRus = ["декабрь", "январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь", "январь"];

var currentType     = "spending";
var type            = "";
var previousMonth   = monthesRus[(new Date()).getMonth()];
var currentMonthRus = monthesRus[(new Date()).getMonth() + 1];
var currentMonth    = monthes[(new Date()).getMonth() + 1];
var nextMonth       = monthesRus[(new Date()).getMonth() + 2];
var identificators  = [0];

$(".back-month-name").text(previousMonth.toUpperCase());
$(".current-month").text(currentMonthRus.toUpperCase());
$(".forward-month-name").text(nextMonth.toUpperCase());

var setCirclesValues = function() {
    $(".spend-info .circle-money-value").text(eachMonthInfo[currentMonth].spendsBalance.toFixed(2) + " $");
    $(".current-info .circle-money-value").text(eachMonthInfo[currentMonth].currentBalance.toFixed(2) + " $");
    $(".get-info .circle-money-value").text(eachMonthInfo[currentMonth].incomeBalance.toFixed(2) + " $");
}

{/* <div class="spending list-group-item d-flex justify-content-between align-items-center rounded-0">
        <div class="d-flex align-items-center">
            <img src="src/img/phone.png">
            <div class="list-item-text">
                <span class="money-value">300.00 $</span> на <span class="spend-type">связь</span>
            </div>
        </div>
        <span aria-hidden="true" class="delete-spending"><img src="src/img/delete.png"></span>
    </div> */}

var createItem = function(config) {
    let item = document.createElement("div"),
        image = document.createElement("img"),
        itemBody = document.createElement("div"),
        mainInfoDiv = document.createElement("div"),
        moneyValue = document.createElement("span"),
        itemType = document.createElement("span"),
        btnDelete = document.createElement("span");
        id = config.id,
        type = config.type,
        typeRus = config.typeRus,
        typeImg = config.typeImg,
        money = config.money,
        pretext = config.pretext;
    
    
    moneyValue.classList.add("money-value");
    moneyValue.innerText = money + " $";

    itemType.classList.add("spend-type");
    itemType.innerText = typeRus;
        
    mainInfoDiv.classList.add("list-item-text");
    mainInfoDiv.append(moneyValue);
    mainInfoDiv.append(document.createTextNode(pretext));
    mainInfoDiv.append(itemType);

    image.setAttribute("src", "src/img/" + typeImg + ".png");

    itemBody.classList.add("d-flex", "align-items-center");
    itemBody.append(image);
    itemBody.append(mainInfoDiv);

    btnDelete.setAttribute("aria-hidden", "true");
    btnDelete.setAttribute("id", "btnDelete" + id)
    btnDelete.classList.add("delete-spending");
    btnDelete.innerHTML = "<img src='src/img/delete.png'>";

    btnDelete.addEventListener("click", function() {
        if (confirm("Точно удалить?")) {
            let self = this;
            eachMonthInfo[currentMonth].list.forEach((element, index) => {
                if (element.id == self.id.slice(9)) {
                    if (element.type == "income") {
                        eachMonthInfo[currentMonth].incomeBalance -= self.parentNode.querySelector(".money-value").innerText.slice(0, self.parentNode.querySelector(".money-value").innerText.length - 2);
                    } else {
                        eachMonthInfo[currentMonth].spendsBalance -= self.parentNode.querySelector(".money-value").innerText.slice(0, self.parentNode.querySelector(".money-value").innerText.length - 2);
                    }
                    eachMonthInfo[currentMonth].currentBalance = eachMonthInfo[currentMonth].incomeBalance - eachMonthInfo[currentMonth].spendsBalance;
                    eachMonthInfo[currentMonth].list.splice(index, 1);
                }
            });
            setCirclesValues();
            setLocalStorage();
            this.parentNode.remove();
        }
    });
    
    item.classList.add(type, "list-group-item", "d-flex", "justify-content-between", "align-items-center", "rounded-0");
    item.style.display = "none";
    item.append(itemBody);
    item.append(btnDelete);

    return item;
}

if (localStorage.getItem("eachMonthInfo")) {
    eachMonthInfo = JSON.parse(localStorage.getItem("eachMonthInfo"));

    eachMonthInfo[currentMonth].list.forEach(element => {
        let item = createItem(element);
        $(".sidebar-list-info").append(item);        
    });
    setCirclesValues();
}
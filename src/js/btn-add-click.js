$(".btn-add").on("click", function() {
    if ($(".main-input").val() != "") {
        let config = {
            id: Math.max(...identificators),
            type: type,
            typeRus: translator[currentType],
            typeImg: currentType,
            money: $(".main-input").val().slice(0, $(".main-input").length - 2),
            pretext: currentType == "income" ? " " : " на ",
        }
        identificators.push(config.id + 1);
        eachMonthInfo[currentMonth].list.unshift(config);
        let item = createItem(config);
        $(".sidebar-list-info").prepend(item);
        if (currentType == "income") {
            eachMonthInfo[currentMonth].incomeBalance += parseFloat(config.money)
            eachMonthInfo[currentMonth].currentBalance += parseFloat(config.money)
        } else {
            eachMonthInfo[currentMonth].spendsBalance += parseFloat(config.money);
            eachMonthInfo[currentMonth].currentBalance -= parseFloat(config.money)
        }
        setCirclesValues();
        setLocalStorage();
        $(".back-to-list-info").click();
    }
})
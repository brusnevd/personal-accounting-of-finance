$(".btn-month-back").hover(
    function() {
        $(".back-month-name").animate({"opacity": ".7"}, 300);
    },
    function() {
        $(".back-month-name").animate({"opacity": ".3"}, 300);
    }
);

$(".btn-month-forward").hover(
    function() {
        $(".forward-month-name").animate({"opacity": ".7"}, 300);
    },
    function() {
        $(".forward-month-name").animate({"opacity": ".3"}, 300);
    }
);
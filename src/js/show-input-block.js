$(".go-to-input").on("click", function() {
    $(".sidebar-list-info").hide();
    $(".container-for-show").slideToggle(500);
    if ($(this).text() == "Добавить доход") {
        $(".info-for-type-title").text("Доходы за месяц");
        $(".container-for-show").css("background", "rgb(199,247,193, .1)");
        $(".main-input-img").attr("src", "src/img/income.png");
        type = "income";
        currentType = "income";
    } else {
        $(".info-for-type-title").text("Расходы за месяц");
        $(".container-for-show").css("background", "rgb(247,172,180, .1)");
        $(".main-input-img").attr("src", "src/img/" + currentType + ".png");
        type = "spending";
    }
    $(".go-to-input").hide();
    $(".back-to-list-info").removeClass("d-none");

    $(".info-for-type-container").children().remove();

    eachMonthInfo[currentMonth].list.forEach(element => {
        if (element.type == type) {
            let item = createItem(element);
            item.querySelector("#btnDelete" + element.id).remove();
            $(".info-for-type-container").append(item);
        }        
    });
})

$(".back-to-list-info").on("click", function() {
    flag = 0;
    symbolsAfterPoint = 0;
    currentType = "spending";
    $(".main-input").val("");
    $(".sidebar-list-info").slideToggle(500);
    $(".container-for-show").hide();
    $(".go-to-input").show();
    $(".back-to-list-info").addClass("d-none");
})
$(".sidebar-btn-up").on("click", function() {
    if ($(".sidebar-btn-up").css("display") != "none") {
        $(".sidebar-btn-up").css("display", "none");
        $(".sidebar").slideToggle(400);
    }
})

$(".sidebar-btn-down").on("click", function() {
    if ($(".sidebar-btn-up").css("display") != "flex") {
        $(".sidebar-btn-up").css("display", "flex");
        $(".sidebar").slideToggle(400);
        if ($(".sidebar-list-info").css("display") == "none") {
            $(".back-to-list-info").click();
        }
    }
})
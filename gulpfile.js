// Подключаем модули галп
const gulp 			= require("gulp"); // подключили галп
const concat 		= require("gulp-concat");
const autoprefixer 	= require("gulp-autoprefixer");
const cleanCSS		= require('gulp-clean-css');
const uglify		= require('gulp-uglify');
const del			= require('del');
const browserSync	= require('browser-sync').create();

// Порядок подключения CSS файлов
// const cssFiles 	= [
// 	'./src/styles/bootstrap.css',
// 	'./src/styles/container.css',
// 	'./src/styles/input.css',
// 	'./src/styles/tasks.css',
// 	'./src/styles/buttons.css',
// 	'./src/styles/media.css'
// ]

// './src/scripts/bootstrap.js.map'

// Порядок подключения JS файлов
// const jsFiles 	= [
// './src/scripts/jquery.js',
// './src/scripts/bootstrap.js',
// 	'./src/scripts/Class.js',
// 	'./src/scripts/AllClick.js',
// 	'./src/scripts/ActiveClick.js',
// 	'./src/scripts/CompletedClick.js',
// 	'./src/scripts/DeleteTasks.js',
// 	'./src/scripts/AddTasks.js',
// 	'./src/scripts/DeleteTaskClick.js',
// 	'./src/scripts/LocalStorage.js',
// ]

// Таск на стили CSS
function styles() {
	// Шаблон для поиска файлов CSS
	// Все файлы по шаблону './src/css/**/*.css'
	return gulp.src('./src/css/*.css')
	// Объединение файлов в один
	.pipe(concat('styles.css'))
	// Добавление префиксов CSS
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	// Минификация CSS
	// .pipe(cleanCSS({
	// 	level: 2
	// }))
	// Выходная папка для стилей
	.pipe(gulp.dest('./build/css'))
	.pipe(browserSync.stream());
}

// Таск на скрипты JS
function scripts() {
	return gulp.src("./src/js/*.js")
	.pipe(concat('script.js'))
	// .pipe(uglify({
	// 	toplevel:true
	// }))
	.pipe(gulp.dest('./build/js'))
	.pipe(browserSync.stream());
}

function clean() {
	return del('build/*')
}

function watch() {
	browserSync.init({
		server: {
			baseDir: "./"
		}
	})
	// Следить за CSS файлами
	gulp.watch('./src/css/*.css', styles)
	// Следить за JS файлами
	gulp.watch("./src/js/*.js", scripts)
	// При изменении HTML запустить синхронизацию
	gulp.watch("./*.html").on('change', browserSync.reload);
}

// Таск вызывающий функцию styles
gulp.task('styles', styles);
// Таск вызывающий функцию scripts
gulp.task('scripts', scripts);
// Таск для очистки папки build
gulp.task('del', clean);
// Таск для отслеживания изменений
gulp.task('watch', watch);
// Таск для удаления файлов в папке build и запуск styles и scripts
gulp.task('build', gulp.series(clean, gulp.parallel(styles, scripts)));
// Таск запускает таск build и watch поледовательно
gulp.task('do', gulp.series('build', 'watch'));